var app = angular.module('app', ["ngRoute" ,"ngMaterial","ngMessages","moment-picker"]);  
  

app.config(['$routeProvider',

     function($routeProvider){
      $routeProvider
      .when("/", {
         templateUrl : "welcome.html",
         controller: "menuCtrl"
       })

       .when("/addPersonButton", {
         templateUrl : "addPersonButton.html",
         controller: "personCtrl"
       })
       .when("/personAdded", {
         templateUrl : "personAdded.html",   
       })
       .when("/personNotAdded", {
         templateUrl : "personNotAdded.html",
       })
       .when("/addRoomButton", {
         templateUrl : "addRoomButton.html",
         controller: "roomCtrl"
       })
       

      .when('/personInfoRoute/:personRouteKey',{
         templateUrl : "person.html",
         controller: "personCtrl"
      })
     
     .when('/seeAllReservations',{
       templateUrl : "seeAllReservations.html",
      controller: "reservationCtrl"
      })
      .when('/roomInfoRoute/:roomRouteKey', {
          templateUrl: "room.html",
         controller: "roomCtrl",
     })
     .when('/reserveOnDates/:roomKey', {
      templateUrl: "reserveOnDates.html",
      controller: "reservationCtrl",
      controller: "menuCtrl"
   })
  
  
      
  }]);

  