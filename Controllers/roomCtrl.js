app.controller('roomCtrl', ['$scope', '$routeParams', 'roomService', '$mdDialog',
    function ($scope, $routeParams, roomService, $mdDialog) {

        var searchKey = Number($routeParams.roomRouteKey);
        $scope.room;
        $scope.rooms;
        $scope.roomAddingStatus = false;
        $scope.roomChanged = false;
        $scope.AroomName = "defaultroom";
        $scope.AroomSize = "defaultroomsize";
        $scope.Akey = 00111;
        let checkRoomName ;
        let checkRoomSize ;
        let checkKey ;



        $scope.findRoom = function (roomKey) {
            roomService.showRoom(roomKey)
                .then(function (response) {
                    $scope.room = response.data;
                    $scope.roomName = $scope.room.roomName;
                }, function (error) {
                    $scope.status = 'Unable to load room data: ' + error.message;
                });
        }

        $scope.addRoom = function (ev) {
            $scope.roomAddingStatus = true;
            newRoom = {
                roomName: $scope.AroomName,
                roomSize: $scope.AroomSize,
                key: $scope.Akey
            }
            roomService.showRoom($scope.Akey)
                .then(function (response) {
                    $scope.checkRoom = response.data;
                    if ($scope.checkRoom.key != $scope.Akey) {
                        $scope.myVar = true;
                        roomService.addRoom(newRoom)
                            .then(function (response) {
                                $scope.roomInsertionstatus = 'added room successfully!';
                                $scope.rooms.push(newRoom);
                            }, function (error) {
                                $scope.roomInsertionstatus = 'unable to add room! :' + error.message;
                            });
                    }
                    else {
                        //$scope.roomInsertionstatus = 'Room with this id is already exist!';
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('Adding Room failed!')
                                .textContent('Room with this id is already exist.')
                                .ariaLabel('Alert')
                                .ok('Got it!')
                                .targetEvent(ev)
                        );
                    }
                },

                    function (error) {
                        $scope.roomInsertionstatus = 'Unable to load room data: ' + error.message;
                    }
                )
        }

        $scope.showRoomInfo = function () {
            roomService.showRoom(searchKey)
                .then(function (response) {
                    $scope.room = response.data;
                    $scope.roomName = $scope.room.roomName;
                    $scope.roomSize = $scope.room.roomSize;
                    $scope.key = $scope.room.key;
                    
                },

                    function (error) {
                        $scope.status = 'Unable to load room data: ' + error.message;
                    }

                );


        }
        $scope.updateRoom = function (roomName, roomSize, key, ev) {
            roomService.showRoom(key)
                .then(function (response) {
                    $scope.room = response.data;
                    checkRoomName = $scope.room.roomName;
                    checkRoomSize = $scope.room.roomSize;
                    checkKey = $scope.room.key;

                    if (checkRoomName == roomName && checkRoomSize == roomSize && checkKey == key) {
                        
                         $mdDialog.show(
                             $mdDialog.alert()
                                 .parent(angular.element(document.querySelector('#popupContainer')))
                                 .clickOutsideToClose(true)
                                 .title('Nothing modified!')
                                 .textContent('You did not change any information about this room!')
                                 .ariaLabel('Alert')
                                 .ok('Got it!')
                                 .targetEvent(ev)
                         );
                        
                     }
                     else {
                        $scope.roomChanged = true;
                        roomModified = {
                            roomName: roomName,
                            roomSize: roomSize,
                            key: key
                        }
                        roomService.updateRoom(roomModified)
                            .then(function (response) {
        
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .parent(angular.element(document.querySelector('#popupContainer')))
                                        .clickOutsideToClose(true)
                                        .title('Room Modified!')
                                        .textContent('You changed the room data!')
                                        .ariaLabel('Alert')
                                        .ok('ok')
                                        .targetEvent(ev)
                                );
                            }, function (error) {
                                $scope.updateStatus = 'Unable to update room : ' + error.message;
                            });
        
                    }


                }, function (error) {
                    $scope.status = 'Unable to load room data: ' + error.message;
                });
                      
        }

    }]);