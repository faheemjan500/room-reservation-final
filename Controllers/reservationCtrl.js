app.controller('reservationCtrl', ['$scope', '$routeParams', 'reservationService', 'personService', 'roomService', '$mdDialog', '$window',
    function ($scope, $routeParams, reservationService, personService, roomService, $mdDialog, $window, ) {

        $scope.currentDate = new Date();
        $scope.startDate = '0';
        $scope.endDate = '0';
        $scope.isOpen = false;
        $scope.isNotEmpty = false;
        $scope.persons = [];
        $scope.ReservationInsertionStatus
        $scope.roomKey = Number($routeParams.roomKey);
        $scope.getRoomName = 'random';
        $scope.room = '';
        


        var flag = 1;
        $scope.personKey;
        $scope.firstName;
        $scope.selectedName = "choose";
        $scope.reservations = [];
        $scope.reservationAdding = false;

        $scope.min = 1;
        $scope.max = 100000000;
        if ($scope.min > $scope.max) {
            [$scope.min, $scope.max] = [$scope.max, $scope.min];
        }

        roomService.showRoom($scope.roomKey)
            .then(function (response) {
                $scope.room = response.data;
                $scope.getRoomName = $scope.room.roomName;
            }, function (error) {
                $scope.status = 'Unable to load person data: ' + error.message;
            });



        $scope.showPersons = function () {
            personService.showPersons()
                .then(function (response) {
                    $scope.persons = response.data;
                }, function (error) {
                    $scope.status = 'Unable to load person data: ' + error.message;
                });
        }






        $scope.addReservation = function (ev) {
            $scope.reservationAdding = true;

            newReservation = {
                key: Math.floor(Math.random() * ($scope.max - $scope.min + 1) + $scope.min),
                personKey: $scope.selectedName,
                roomKey: $scope.roomKey,
                startTime: $scope.startDate,
                endTime: $scope.endDate,

            }


            if (newReservation.startTime == 0 || newReservation.endTime == 0) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Time error')
                        .textContent('You have to chose both start and end time!')
                        .ariaLabel('Alert')
                        .ok('Got it!')
                        .targetEvent(ev)
                );
            }
            else if (newReservation.startTime < $scope.currentDate || newReservation.endTime < $scope.currentDate) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Cannot reserve room for these timings')
                        .textContent('You are living in the past!')
                        .ariaLabel('Alert')
                        .ok('Got it!')
                        .targetEvent(ev)
                );
            }
            else if ($scope.selectedName == "choose") {

                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('select person')
                        .textContent('Please select person from the list!')
                        .ariaLabel('Alert')
                        .ok('ok')
                        .targetEvent(ev)
                );

            }
            else {
                reservationService.addReservation(newReservation)
                    .then(function (response) {
                        $scope.reservations.push(newReservation);
                        $scope.ReservationInsertionStatus = 'reservation inserted successfully!';
                    }, function (error) {
                        $scope.ReservationInsertionStatus = 'cannot make reservation at the specified times! ';
                    });
            }


        }

        $scope.deleteAllReservations = function (ev) {

            var confirm = $mdDialog.confirm()
                .title('Are you sure?')
                .textContent('You are about to remove all the reservations.')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('No');

            $mdDialog.show(confirm).then(function () {
                $scope.status = 'You decided to remove all reservations.';
                reservationService.showReservations()
                    .then(function (response) {
                        $scope.reservations = response.data;
                        if ($scope.reservations.length != 0) {

                            reservationService.deleteAllReservations()
                                .then(function (response) {
                                    $scope.reservations = response.data;
                                    $scope.deleteAllStatus = 'Removed All reservations ';

                                }, function (error) {
                                    $scope.deleteAllStatus = 'Unable to delete reservations: ' + error.message;
                                }
                                )
                        } else {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .parent(angular.element(document.querySelector('#popupContainer')))
                                    .clickOutsideToClose(true)
                                    .title('Alert message!')
                                    .textContent('There is no reservation in the system.')
                                    .ariaLabel('Alert')
                                    .ok('Got it!')
                                    .targetEvent(ev)
                            );
                        }

                    }, function (error) {
                        $scope.status = 'Unable to load reservations: ' + error.message;
                    }
                    )



            }, function () {
                $scope.status = 'You decided to keep all reservations.';
            });
        };



        $scope.showReservations = function () {

            $scope.showAllReservations = true;
            reservationService.showReservations()
                .then(function (response) {
                    $scope.reservations = response.data;
                    if ($scope.reservations.length != 0) {
                        $scope.isNotEmpty = true;
                    }


                }, function (error) {
                    $scope.status = 'Unable to load reservations: ' + error.message;
                }
                )
        };
        $scope.deleteReservation = function (ev, key) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure?')
                .textContent('you are about to delete the reservation.')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('No');

            $mdDialog.show(confirm).then(function () {
                $scope.status = 'You decided to delete the reservation.';
                reservationService.deleteReservation(key).then(function (response) {
                    $scope.status = 'reservation removed from the list';
                    for (var i = 0; i < $scope.reservations.length; i++) {

                        if ($scope.reservations[i].key === key) {
                            $scope.reservations.splice(i, 1);
                            break;
                        }
                    }
                }, function (error) {
                    $scope.status = 'unable to remove reservation!' + error.message;
                });




            }, function () {
                $scope.status = 'You decided to keep it.';
            });
        };




    }]);