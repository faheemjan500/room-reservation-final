app.controller('menuCtrl', ['$scope', 'personService', 'roomService', '$mdDialog',
    function ($scope, personService, roomService, $mdDialog) {


        $scope.status = '  ';
        $scope.status;
        $scope.persons;
        $scope.rooms;
        $scope.showAllPersons = true;
        $scope.showAllRooms = false;


        $scope.showPersons = function () {
            $scope.showAllPersons = !$scope.showAllPersons;
            $scope.showAllRooms = false;

        }
        $scope.showRooms = function () {
            $scope.showAllRooms = !$scope.showAllRooms;
            $scope.showAllPersons = false;
        }
        personService.showPersons()
            .then(function (response) {
                $scope.persons = response.data;
            }, function (error) {
                $scope.status = 'Unable to load person data: ' + error.message;
            });


        roomService.showRooms()
            .then(function (response) {
                $scope.rooms = response.data;
            }, function (error) {
                $scope.status = 'Unable to load room data: ' + error.message;
            });

           //delete person
        $scope.deletePerson = function (ev,key) {
            var confirm = $mdDialog.confirm()
                .title('are you sure?')
                .textContent('You are going to delete the person.')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('No');

            $mdDialog.show(confirm).then(function () {
                $scope.status = 'You decided to delete.';

                personService.deletePerson(key)
                    .then(function (response) {
                        $scope.status = 'Deleted person! Refreshing persons list.';
                        for (var i = 0; i < $scope.persons.length; i++) {

                            if ($scope.persons[i].key === key) {
                                $scope.persons.splice(i, 1);
                                break;
                            }
                        }

                    }, function (error) {
                        $scope.status = 'Unable to delete person: ' + error.message;
                    });

            },



                function () {
                    $scope.status = 'You decided not to delete.';
                });
        };
        //delete room
        $scope.deleteRoom = function (ev, key) {
            var confirm = $mdDialog.confirm()
                .title('are you sure?')
                .textContent('You are going to delete the room.')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('No');

            $mdDialog.show(confirm).then(function () {
                $scope.status = 'You decided to delete.';
                roomService.deleteRoom(key).then(function (response) {
                    $scope.status = 'Deleted room! Refreshing rooms list.';
                    for (var i = 0; i < $scope.rooms.length; i++) {

                        if ($scope.rooms[i].key === key) {
                            $scope.rooms.splice(i, 1);
                            break;
                        }
                    }

                }, function (error) {
                    $scope.status = 'Unable to delete room: ' + error.message;
                });
            },
                function () {
                    $scope.status = 'You decided not to delete.';
                });
        };



















    }])