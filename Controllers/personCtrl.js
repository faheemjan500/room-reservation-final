app.controller('personCtrl', ['$scope', '$routeParams', 'personService','$mdDialog',
    function ($scope, $routeParams, personService,$mdDialog) {

        $scope.searchKey = Number($routeParams.personRouteKey);
        $scope.person;
        $scope.persons;
        $scope.myVar = false;
        $scope.personAddingStatus = false;
        $scope.personChanged = false;       
        $scope.AfirstName = "abc";
        $scope.AlastName = "xyz";
        $scope.Akey = 1235;


        $scope.findPerson = function (personKey) {
            personService.showPerson(personKey)
                .then(function (response) {
                    $scope.person = response.data;
                    $scope.firstName = $scope.person.firstName;
                }, function (error) {
                    $scope.status = 'Unable to load person data: ' + error.message;
                });
        }


        $scope.addPerson = function (ev) {
            $scope.personAddingStatus = true;
            newPerson = {
                firstName: $scope.AfirstName,
                lastName: $scope.AlastName,
                key: $scope.Akey
            }

            personService.showPerson($scope.Akey)
                .then(function (response) {
                    $scope.checkPerson = response.data;
                    if ($scope.checkPerson.key != $scope.Akey) {
                        $scope.myVar = true;
                        personService.addPerson(newPerson)
                            .then(function (response) {
                                $scope.personInsertionstatus = 'added person successfully!';
                                $scope.persons.push(newPerson);
                            }, function (error) {
                                $scope.personInsertionstatus = 'unable to add person! :' + error.message;
                            });
                    }
                    else {
                        $mdDialog.show(
                            $mdDialog.alert()
                              .parent(angular.element(document.querySelector('#popupContainer')))
                              .clickOutsideToClose(true)
                              .title('Adding person failed!')
                              .textContent('Person with this Codice Fiscale is already exists!')
                              .ariaLabel('Alert')
                              .ok('Got it!')
                              .targetEvent(ev)
                          );

                    }
                }, function (error) {
                    $scope.personInsertionstatus = 'Unable to load person data: ' + error.message;
                }

                )



        }



        $scope.showPersonInfo = function () {
            personService.showPerson($scope.searchKey)
                .then(function (response) {
                    $scope.person = response.data;
                    $scope.firstName = $scope.person.firstName;
                    $scope.lastName = $scope.person.lastName;
                    $scope.key = $scope.person.key;
                }, function (error) {
                    $scope.status = 'Unable to load person data: ' + error.message;
                });



        }
        $scope.updatePerson = function(firstName, lastName, key,ev) {
            personService.showPerson(key)
            .then(function (response) {
                $scope.person = response.data;
                checkFirstName = $scope.person.firstName;
                checkLastName = $scope.person.lastName;
                checkKey = $scope.person.key;
                if(checkFirstName==firstName && checkLastName==lastName && checkKey==key )
                {
              $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Nothing modified!')
                    .textContent('You did not change any information about this person!')
                    .ariaLabel('Alert')
                    .ok('Got it!')
                    .targetEvent(ev)
                );
                }
             else {
              personModified = {
                       firstName : firstName,
                       lastName :  lastName,
                       key : key
               }
                personService.updatePerson(personModified)
                  .then(function (response) {
  
                      $mdDialog.show(
                          $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Person modified!')
                            .textContent('You changed the person data!')
                            .ariaLabel('Alert')
                            .ok('ok')
                            .targetEvent(ev)
                        );
                  }, function (error) {
                      $scope.updateStatus = 'Unable to update person : ' + error.message;
                  });
  
              }
            }, function (error) {
                $scope.status = 'Unable to load person data: ' + error.message;
            });
        
       
        }

        

    }]);