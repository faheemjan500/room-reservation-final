angular.module('app')
.factory('reservationService', ['$http', function ($http) {
 
    var reservationService = {};


    reservationService.addReservation = function(newReservation){
        var urlBase = 'http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/reservation/setData';
        return $http.post(urlBase, newReservation);

        //return $http.post('http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/reservation/setData', newReservation);

    };
    reservationService.deleteReservation = function (key) {
        var urlBase =  'http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/reservation/deleteReservation'; 
          return $http.delete(urlBase + '/' + key);
  
      };
      reservationService.deleteAllReservations = function (){
        return $http.delete('http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/reservation/deleteAllReservations');
    };
      reservationService.showReservations = function (){
        return $http.get('http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/reservation/getAllData');
    };
    reservationService.showReservation = function (key){
        var urlBase =  'http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/reservation/getData'; 
        return $http.get(urlBase + '/' + key);
    };
    
    
    return  reservationService;
}]);