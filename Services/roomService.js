angular.module('app')
.factory('roomService', ['$http', function ($http) {
 
    var roomService = {};


    roomService.showRooms = function (){
        return $http.get('http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/room/getAllRoomData');
    };


    roomService.deleteRoom = function (key) {
      var urlBase =  'http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/room/deleteRoom'; 
        return $http.delete(urlBase + '/' + key);

    };


    roomService.addRoom = function (newRoom) {
        return $http.post('http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/room/setData', newRoom);

    };
    roomService.showRoom = function (key){
        var urlBase =  'http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/room/getData'; 
        return $http.get(urlBase + '/' + key);
    };
    roomService.updateRoom = function(roomModified){
        var urlBase = 'http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/room/updateData';
        return $http.put(urlBase, roomModified);


    };
    return  roomService;
}]);